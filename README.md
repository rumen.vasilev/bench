# bench

CPU / Memory benchmark tool

## Usage

```
demo@user % ./bench 
  -cpu
    	Do a cpu utilisation test
  -cpu-limit int
    	input how many cpus do you want to utilise at 100% (default 1)
  -mem
    	Do a memory pressure test
  -mem-limit int
    	input the amount of memory you want to ocupy in megabytes (500 = 500M, 1000 = 1G, etc) (default 1000)
```

Or if you're using the docker container:

```
# to run the cpu test on 1 core
demo@user % docker run -it --rm bench -cpu
# to run the cpu test on 2 cores
demo@user % docker run -it --rm bench -cpu -cpu-limit 2
# to run the mem test with 1G allocation
demo@user % docker run -it --rm bench -mem -mem-limit 1000
```

Keep in mind the memory allocation is always a little over what you specify. E.g. 500 is not exactly 500M, but 501M.

## Building it

You can either build it locally with go, or with docker.

If locally, you need minimum go version go1.22. Get one from [here](https://go.dev/doc/install).

```
go build .
```

With docker:

```
docker build -t bench .
```
