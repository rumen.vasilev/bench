package main

import (
	"flag"
	"sync"
	"time"
)

var (
	cpu      bool
	cpuLimit int
	mem      bool
	memLimit int
)

func main() {
	parseFlags()

	if !cpu && !mem {
		flag.PrintDefaults()
		return
	}

	if cpu {
		runCPUTest(cpuLimit)
	}

	if mem {
		runMemTest(memLimit)
	}
}

func parseFlags() {
	flag.BoolVar(&mem, "mem", false, "Do a memory pressure test")
	flag.IntVar(&memLimit, "mem-limit", 1000, "input the amount of memory you want to ocupy in megabytes (500 = 500M, 1000 = 1G, etc)")
	flag.BoolVar(&cpu, "cpu", false, "Do a cpu utilisation test")
	flag.IntVar(&cpuLimit, "cpu-limit", 1, "input how many cpus do you want to utilise at 100%")

	flag.Parse()
}

func runCPUTest(cpus int) {
	var wg sync.WaitGroup
	for range cpus {
		wg.Add(1)
		go cpuJob()
	}
	wg.Wait()
}

func cpuJob() {
	for {
	}
}

func runMemTest(memSize int) {
	m := memSize * 1024 * 1024 * 1000
	m = m - m/5
	_ = make([]byte, m)
	for {
		time.Sleep(5 * time.Second)
	}
}
