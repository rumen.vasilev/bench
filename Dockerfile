FROM golang:1.22 as builder
COPY . /build
WORKDIR /build
RUN go build .

FROM scratch
COPY --from=builder /build/bench /bench
ENTRYPOINT ["/bench"]
